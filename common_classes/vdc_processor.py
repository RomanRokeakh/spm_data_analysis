import os
from common_classes.parameters import Parameters
from common_classes.fitter import Fitter
from common_classes.filtrator import Filtrator
from common_classes.plotter import Plotter
from common_classes.dir_processor import DirProcessor
from commonmethods.utility_methods import list_dirs, get_bin_files_in_dir
import time
import numpy as np
from scipy.signal import medfilt


class OutputOptions:
    def __init__(self, fit_vals=False, fit_graphs=False, Amax=False, A0=False, w_res=False, phase=False, Q=False):
        self.fit_vals = fit_vals
        self.fit_graphs = fit_graphs
        self.Amax = Amax
        self.A0 = A0
        self.w_res = w_res
        self.phase = phase
        self.Q = Q

    def list_options(self):
        options_list = {'fits': [], 'relationships': []}
        if self.fit_vals:
            options_list['fits'].append('fit_vals')
        if self.fit_graphs:
            options_list['fits'].append('fit_graphs')
        if self.Amax:
            options_list['relationships'].append('Amax')
        if self.A0:
            options_list['relationships'].append('A0')
        if self.w_res:
            options_list['relationships'].append('w_res')
        if self.phase:
            options_list['relationships'].append('phase')
        if self.Q:
            options_list['relationships'].append('Q')
        return options_list


class VdcProcessor:
    def __init__(self, input_dirs, output_dir, vdc, filtration_start, output_options, filt_window=None):
        if isinstance(input_dirs, str):
            self.dirs_to_process = list_dirs(input_dirs)
        else:
            self.dirs_to_process = input_dirs

        self.output_dir = output_dir
        self.vdc = vdc

        self.output_options = output_options
        self.output_dirs = self.prepare_output_dirs()

        parameters = Parameters.read_from_file(os.path.join(self.dirs_to_process[0], 'paramiters.txt'))
        fitter = Fitter(parameters)
        filtrator = Filtrator(filtration_start, parameters.bins_number, filt_window)
        plotter = Plotter()
        self.dir_processor = DirProcessor(filtrator=filtrator,
                                          fitter=fitter,
                                          plotter=plotter,
                                          vdc=vdc,
                                          output_dirs=self.output_dirs
                                          )

    def prepare_output_dirs(self, output_root=None, output_options=None):
        if output_root is None:
            output_root = self.output_dir
        if output_options is None:
            output_options = self.output_options

        output_dirs = {'output_root': output_root}
        output_options_list = output_options.list_options()
        if output_options_list['fits']:
            output_dirs['fits_output'] = os.path.join(output_root, 'fits')
            for key in output_options_list['fits']:
                output_dirs[key] = os.path.join(output_root, key)

        output_dirs['steps_output'] = os.path.join(output_root, 'steps')
        output_dirs['steps_graphs'] = os.path.join(output_dirs['steps_output'], 'graphs')
        output_dirs['steps_vals'] = os.path.join(output_dirs['steps_output'], 'vals')
        output_dirs['zeros_output'] = os.path.join(output_root, 'zeros')
        output_dirs['zeros_graphs'] = os.path.join(output_dirs['zeros_output'], 'graphs')
        output_dirs['zeros_vals'] = os.path.join(output_dirs['zeros_output'], 'vals')
        for key in output_options_list['relationships']:
            for name in ['steps_graphs', 'steps_vals', 'zeros_graphs', 'zeros_vals']:
                output_dirs['{}_{}'.format(name, key)] = os.path.join(output_dirs[name], key)

        for key in output_dirs:
            os.makedirs(output_dirs[key], exist_ok=True)

        return output_dirs

    def process(self, average_cycles=False, filt_window=1):
        for current_dir in self.dirs_to_process:
            start_time = time.perf_counter()
            relative_current_dir = os.path.basename(os.path.normpath(current_dir))
            filenames = get_bin_files_in_dir(os.path.join(current_dir, 'fourier_amp_ph'))
            if len(filenames) == 0:
                continue
            param_steps_zeros_all = self.dir_processor.process_dir(filenames, relative_current_dir)

            for key in self.output_options.list_options()['relationships']:
                step_vals_dir_name = self.output_dirs['steps_vals_{}'.format(key)]
                step_graph_dir_name = self.output_dirs['steps_graphs_{}'.format(key)]
                zeros_vals_dir_name = self.output_dirs['zeros_vals_{}'.format(key)]
                zeros_graph_dir_name = self.output_dirs['zeros_graphs_{}'.format(key)]
                step_zeros_all = [(medfilt(x_steps, filt_window), medfilt(x_zeros, filt_window), [])
                                  for x_steps, x_zeros, _ in param_steps_zeros_all[key]]
                if len(step_zeros_all) == 1:
                    x_steps, x_zeros, _ = step_zeros_all[0]
                    triplets = [(x_steps, step_vals_dir_name, step_graph_dir_name),
                                (x_zeros, zeros_vals_dir_name, zeros_graph_dir_name)]
                    for x, vals_dir_name, graph_dir_name in triplets:
                        needed_vdc = self.vdc.step_values[self.vdc.get_cycle_step_indices(0)]
                        self._save_graph_and_vals(x,
                                                  needed_vdc,
                                                  vals_dir_name,
                                                  graph_dir_name,
                                                  relative_current_dir)
                else:
                    final_paths = [os.path.join(dir_name, relative_current_dir)
                                   for dir_name in [step_vals_dir_name,
                                                    step_graph_dir_name,
                                                    zeros_vals_dir_name,
                                                    zeros_graph_dir_name]]
                    for path in final_paths:
                        os.makedirs(path, exist_ok=True)
                    for i, cycle in enumerate(step_zeros_all):
                        x_steps, x_zeros, _ = cycle
                        triplets = [(x_steps, *final_paths[:2]),
                                    (x_zeros, *final_paths[2:])]
                        for x, vals_dir_name, graph_dir_name in triplets:
                            needed_vdc = self.vdc.step_values[self.vdc.get_cycle_step_indices(i)]
                            if len(x) - len(needed_vdc) != 0:
                                raise ValueError
                            self._save_graph_and_vals(x,
                                                      needed_vdc,
                                                      vals_dir_name, graph_dir_name,
                                                      'cycle {}'.format(i))
                    if average_cycles:
                        step_cycles = []
                        zeros_cycles = []
                        for x_steps, x_zeros, _ in step_zeros_all:
                            step_cycles.append(x_steps)
                            zeros_cycles.append(x_zeros)
                        x_steps = np.array(step_cycles).mean(axis=0)
                        x_zeros = np.array(zeros_cycles).mean(axis=0)
                        triplets = [(x_steps, *final_paths[:2]),
                                    (x_zeros, *final_paths[2:])]
                        for x, vals_dir_name, graph_dir_name in triplets:
                            needed_vdc = self.vdc.step_values[self.vdc.get_cycle_step_indices(0)]
                            self._save_graph_and_vals(x,
                                                      needed_vdc,
                                                      vals_dir_name, graph_dir_name,
                                                      'mean')

            end_time = time.perf_counter()
            print(relative_current_dir, end_time-start_time)

    def _save_graph_and_vals(self, x, y, vals_dir_name, graph_dir_name, name):
        self.dir_processor.plotter.plot_param_vdc_function(
            x,
            y,
            os.path.join(graph_dir_name, '{}'.format(name))
        )
        filename = os.path.join(vals_dir_name, '{}.txt'.format(name))
        with open(filename, 'w') as f:
            f.write(' '.join([str(val) for val in x]))