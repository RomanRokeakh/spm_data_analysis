import numpy as np
from common_classes.models import Model, ModelParams
from scipy.optimize import curve_fit


def fwhm(x, y, frac):
    d = y - (y.max() / frac)
    indexes = np.where(d > 0)[0]
    return np.abs(x[indexes[-1]] - x[indexes[0]])


class Fitter:
    def __init__(self, fitter_params):
        """
        input: parameters which are same for all of the data fitting
        """
        self.params = fitter_params

    def get_init_vals(self, meas):
        peak_ind = np.argmax(meas.amp)
        Amax = meas.amp[peak_ind]
        w_res = meas.freq[peak_ind]
        Q = w_res / fwhm(meas.freq, meas.amp, 2)
        A0 = 0
        return ModelParams(Amax=Amax, w_res=w_res, Q=Q, A0=A0)

    def fit(self, meas, maxfev=800):
        """
        :input:
        measurement
        :return:
        model parameters fitted to the measurement
        """
        init_vals = self.get_init_vals(meas)
        prefit = self._prefit(meas, init_vals, maxfev)

        fixed_A0 = Model.fix_A0(prefit.A0)
        init_list = [init_vals.Amax, init_vals.w_res, init_vals.Q]
        est = self.save_fit(fixed_A0, meas.freq, meas.amp, init_list, maxfev=maxfev)
        Amax_fit, w_res_fit, Q_fit = est
        afterfit = ModelParams(Amax=Amax_fit, w_res=w_res_fit, Q=Q_fit)

        fit_result = self._connect_fits(afterfit=afterfit, prefit=prefit, init_vals=init_vals)
        # select phase from left side
        meas.phase = np.unwrap(meas.phase)
        closest_w_index = np.abs(meas.freq-fit_result.w_res).argmin()
        phase = meas.phase[closest_w_index]
        #phase = Model.phase_func(meas.freq[closest_w_index], fit_result.w_res, fit_result.Q)
        #phase = Model.phase_func(meas.freq, fit_result.w_res, fit_result.Q)[closest_w_index]
        fit_result.phase = phase
        return fit_result

    def _prefit(self, meas, init_vals, maxfev):
        fixed_Amax = Model.fix_Amax(init_vals.Amax)
        init_list = [init_vals.w_res, init_vals.Q, init_vals.A0]
        est = self.save_fit(fixed_Amax, meas.freq, meas.amp, init_list, maxfev=maxfev)
        w_res_prefit, Q_prefit, A0_prefit = est
        return ModelParams(w_res=w_res_prefit, Q=Q_prefit, A0=A0_prefit)

    def _is_outlier(self, afterfit, prefit, init_Amax):
        return ((abs(afterfit.Amax) > 2 * init_Amax) or
                (abs(afterfit.Q) > 1000) or
                (afterfit.w_res > self.params.max_freq / self.params.sample_rate) or
                (afterfit.w_res < self.params.min_freq / self.params.sample_rate) or
                (afterfit.Q * afterfit.Amax < 0) or
                (afterfit.Amax < prefit.A0))

    def _connect_fits(self, afterfit, prefit, init_vals):
        if self._is_outlier(afterfit, prefit, init_vals.Amax):
            Amax = init_vals.Amax
            w_res = prefit.w_res
            Q = prefit.Q if prefit.Q < 1000 else np.NaN
        else:
            Amax = afterfit.Amax
            w_res = afterfit.w_res
            Q = afterfit.Q
        A0 = prefit.A0
        return ModelParams(Amax=Amax, w_res=w_res, Q=Q, A0=A0)

    def save_fit(self, func, x, y, init_vals, maxfev=800):
        try:
            est, cov = curve_fit(func, x, y, init_vals, maxfev=maxfev)
        except RuntimeError:
            est = np.zeros(len(init_vals))
            est[:] = np.nan
        return est

    def eval_fitted_amp(self, meas, fit_res):
        amp = Model.amp_func(meas.freq, Amax=fit_res.Amax, w_res=fit_res.w_res, Q=fit_res.Q, A0=fit_res.A0)
        return amp
