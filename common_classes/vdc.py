from itertools import chain, repeat
import numpy as np
import abc


def monotonic(amp_start, amp_end, steps, points_per_step, points_per_zero, replace_zeros=False):
    d_amp = (amp_end - amp_start) / steps
    return np.array(
        list(
            chain.from_iterable(
                [_full_step_with_zeros(
                    amp_start + i * d_amp,
                    points_per_step,
                    points_per_zero,
                    replace_zeros
                ) for i in range(steps + 1)])))


def _full_step_with_zeros(amp, points_per_step, points_per_zero, replace_zeros=False):
    zeros_amp = amp if replace_zeros else 0
    return chain.from_iterable(
        [repeat(amp, points_per_step),
         repeat(zeros_amp, points_per_zero)]
    )


class Signal(abc.ABC):

    def __init__(self,
                 steps_per_grows,
                 points_per_step,
                 points_per_zero,
                 number_of_cycles,
                 amp_start,
                 amp_end,
                 bipolar,
                 replace_zeros=True):
        if not bipolar in {-1, 1}:
            raise ValueError("bipolar parameter should be -1 or 1\nbipolar={}".format(bipolar))
        self.steps_per_grows = steps_per_grows
        self.points_per_step = points_per_step
        self.points_per_zero = points_per_zero
        self.number_of_cycles = number_of_cycles
        self.amp_start = abs(amp_start)
        self.amp_end = abs(amp_end)
        self.bipolar = bipolar
        self.replace_zeros = replace_zeros
        self.voltage, self.cycles_indices = self._number_to_voltage()

        self.steps_indices = []
        self.zeros_indices = []
        self._set_ranges()
        self.step_values = list(self._get_step_values())

    @abc.abstractmethod
    def _number_to_voltage(self):
        pass

    @abc.abstractmethod
    def steps_per_cycle(self):
        pass

    def _set_ranges(self):
        i = 0
        while i < len(self.voltage):
            self.steps_indices.append(slice(i, i + self.points_per_step))
            i += self.points_per_step
            self.zeros_indices.append(slice(i, i + self.points_per_zero) if self.points_per_zero else slice(0, 0))
            i += self.points_per_zero

    def _get_step_values(self):
        for slice_i in self.steps_indices:
            yield self.voltage[slice_i][0]

    @abc.abstractmethod
    def get_cycle_step_indices(self, i):
        pass



class EFPFMSignal(Signal):

    def _number_to_voltage(self):
        full_step_len = self.points_per_step + self.points_per_zero
        cycles = []
        cycles_indices = []
        current_position = 0
        for i in range(self.number_of_cycles):
            #cycle = self.bipolar * ((-1) ** i) * np.array(
            cycle = self.bipolar * np.array(
                        monotonic(-self.amp_start, self.amp_start,
                                  2 * self.steps_per_grows,
                                  self.points_per_step,
                                  self.points_per_zero)
            )
            cycles_indices.append(slice(current_position, current_position + len(cycle)))
            current_position += len(cycle)
            # current_position += len(cycle - full_step_len)
            #cycles.append(cycle[:-full_step_len])
            cycles.append(cycle)
        # cycles.append(_full_step_with_zeros(self.amp_start * self.bipolar * ((-1) ** (self.number_of_cycles - 1)),
        #                                     self.points_per_step,
        #                                     self.points_per_zero))
        return list(chain.from_iterable(cycles)), cycles_indices

    def steps_per_cycle(self):
        return 2*self.steps_per_grows + 1

    def get_cycle_step_indices(self, i):
        if i > self.number_of_cycles - 1:
            raise ValueError('{} is out of range 0-{}'.format(i, self.number_of_cycles-1))
        return slice(i * self.steps_per_cycle(),
                     (i + 1) * self.steps_per_cycle())


class SSPFMSignal(Signal):

    def _number_to_voltage(self):
        cycle_grow = (self.amp_end - self.amp_start) / self.number_of_cycles
        full_step_len = self.points_per_step + self.points_per_zero
        cycles = []
        cycles_indices = []
        current_position = 0
        for i in range(self.number_of_cycles):
            cycle_amp = self.amp_start + i * cycle_grow
            cycle = self.bipolar * np.array(
                list(
                    chain(
                        monotonic(
                            0, cycle_amp,
                            self.steps_per_grows,
                            self.points_per_step,
                            self.points_per_zero,
                            self.replace_zeros
                        ),
                        monotonic(
                            cycle_amp, 0,
                            self.steps_per_grows,
                            self.points_per_step,
                            self.points_per_zero,
                            self.replace_zeros
                        )[full_step_len:],
                        monotonic(
                            0, -cycle_amp,
                            self.steps_per_grows,
                            self.points_per_step,
                            self.points_per_zero,
                            self.replace_zeros
                        )[full_step_len:],
                        monotonic(
                            -cycle_amp, 0,
                            self.steps_per_grows,
                            self.points_per_step,
                            self.points_per_zero,
                            self.replace_zeros
                        )[full_step_len:])))
            cycles_indices.append(slice(current_position, current_position + len(cycle)))
            cycles.append(cycle[:-full_step_len])
            current_position += len(cycle) - full_step_len
        cycles.append(_full_step_with_zeros(0,
                                            self.points_per_step,
                                            self.points_per_zero,
                                            self.replace_zeros))
        return list(chain.from_iterable(cycles)), cycles_indices

    def steps_per_cycle(self):
        return 4 * self.steps_per_grows + 1

    def get_cycle_step_indices(self, i):
        if i > self.number_of_cycles - 1:
            raise ValueError('{} is out of range 0-{}'.format(i, self.number_of_cycles-1))
        return slice(i * self.steps_per_cycle() - (i > 0),
                     (i + 1) * self.steps_per_cycle())
