class Parameters:
    def __init__(self, min_freq, max_freq, sample_rate, bins_number, lines_number, points_number):
        self.min_freq = min_freq
        self.max_freq = max_freq
        self.sample_rate = sample_rate
        self.bins_number = bins_number
        self.lines_number = lines_number
        self.points_number = points_number

    @staticmethod
    def read_from_file(filename):
        """returns raw string dictionary and some prepared values"""
        with open(filename, 'r') as file:
            input_string = file.read()

        raw_parameters = {}
        for line in input_string.strip().split('\n'):
            name, value = Parameters._line_to_name_value_pair(line)
            raw_parameters[name] = value

        min_freq = 1000 * float(raw_parameters['frequency min, kHz'].replace(',', '.'))
        max_freq = 1000 * float(raw_parameters['frequency max, kHz'].replace(',', '.'))
        sample_rate = float(
            raw_parameters['Acquisition sample rate, S/s'].replace(',', '.'))
        bins_number = int(raw_parameters['bins number'])
        lines_number = int(raw_parameters['scan lines number'])
        points_number = int(raw_parameters['scan points in line number'])

        parameters = Parameters(
            min_freq=min_freq,
            max_freq=max_freq,
            sample_rate=sample_rate,
            bins_number=bins_number,
            lines_number=lines_number,
            points_number=points_number
        )

        return parameters

    @staticmethod
    def _line_to_name_value_pair(line):
        return [string[::-1] for string in line[::-1].split(maxsplit=1)][::-1]
        # return list(map(lambda string: string[::-1],
        #                 line[::-1].split(maxsplit=1)))[::-1]
