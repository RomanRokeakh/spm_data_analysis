from common_classes.measurement import Measurement
from scipy.signal import medfilt


class Filtrator:
    def __init__(self, start, step, filt_window=None):
        self.start = start
        self.step = step
        if filt_window is None:
            self.filt_window = {'amp': 1, 'phase': 1}
        else:
            self.filt_window = filt_window

    def filter(self, array, start=None):
        if start is None:
            start = self.start
        return array[slice(start, -1, self.step + 1)]

    def filter_measurement(self, data, start=None):
        f_amp = self.filter(medfilt(data.amp, self.filt_window['amp']), start)
        f_freq = self.filter(data.freq, start)
        f_phase = self.filter(medfilt(data.phase, self.filt_window['phase']), start)
        return Measurement(freq=f_freq, amp=f_amp, phase=f_phase)
