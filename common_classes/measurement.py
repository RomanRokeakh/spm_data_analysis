import numpy as np


class Measurement:
    def __init__(self, freq, amp, phase):
        self.freq = freq
        self.amp = amp
        self.phase = phase

    @staticmethod
    def read_from_file(filename):
        """
        freq, amp, phase - this order is very important
        :param filename:
        :return:
        """
        numbers = np.fromfile(filename, np.dtype('<f'))
        array = np.reshape(numbers, (3, len(numbers) // 3))
        freq = array[0]
        amp = array[1]
        phase = array[2]

        measurement = Measurement(freq=freq, amp=amp, phase=phase)
        return measurement
