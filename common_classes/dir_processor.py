import os
import multiprocessing as mp
import warnings
import numpy as np
from common_classes.measurement import Measurement
from common_classes.models import ModelParams
from commonmethods.utility_methods import save_fit_1arg, get_bin_files_in_dir


class DirProcessor:
    def __init__(self, filtrator, fitter, plotter, vdc, output_dirs):
        """
        input: parameters which are same for all of the data processing
        """
        self.filtrator = filtrator
        self.fitter = fitter
        self.plotter = plotter
        self.vdc = vdc
        self.fit_vals_dir = output_dirs.get('fit_vals')
        self.fit_graph_dir = output_dirs.get('fit_graphs')

    def process_dir(self, filenames, relative_dir_name):
        """
        input: dir_name
        saves
        :return:
        returns two lists. one contains fits corresponding to 0 VDC, another to pitched VDC.
        saves graphs in the process
        """
        with mp.Pool(mp.cpu_count()) as p:
            data_list = p.map(Measurement.read_from_file, filenames)
            filtered_data = p.map(self.filtrator.filter_measurement, data_list)
            fits = p.map(self.fitter.fit, filtered_data)
            if self.fit_vals_dir is not None:
                fit_vals_dir = os.path.join(self.fit_vals_dir, relative_dir_name)
                os.makedirs(fit_vals_dir, exist_ok=True)
                tuples = [('{}.txt'.format(os.path.join(fit_vals_dir, str(i))), fits[i]) for i in range(len(fits))]
                p.map(save_fit_1arg, tuples)
            if self.fit_graph_dir is not None:
                fit_graph_dir = os.path.join(self.fit_graph_dir, relative_dir_name)
                os.makedirs(fit_graph_dir, exist_ok=True)
                fitted_data = [self.fitter.eval_fitted_amp(data_list[i], fits[i]) for i in range(len(data_list))]
                tuples = [
                    (data_list[i],
                     filtered_data[i],
                     fitted_data[i],
                     os.path.join(fit_graph_dir, str(i)))
                    for i in range(len(fits))]
                p.map(self.plotter.plot_fit_1arg, tuples)
            fits_array = np.array([fit.as_list() for fit in np.array(fits)])
            param_names = ModelParams.params_list()
            param_steps_zeros_all = {param_names[i]: list(self.get_mean_for_same_vdc(fits_array[:, i]))
                                     for i in range(len(param_names))}
            return param_steps_zeros_all

    def get_mean_for_same_vdc(self, x):
        step_chunks = [x[slice_i] for slice_i in self.vdc.steps_indices]
        zeros_chunks = [x[slice_i] for slice_i in self.vdc.zeros_indices]

        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            step_means = [np.nanmean(chunk) for chunk in step_chunks]
            zeros_means = [np.nanmean(chunk) for chunk in zeros_chunks]
        for i in range(self.vdc.number_of_cycles):
            slice_i = self.vdc.get_cycle_step_indices(i)
            yield step_means[slice_i], zeros_means[slice_i], []  # last one should be both combined
