import os
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt


class Plotter:
    def __init__(self):
        """
        might be some style settings
        """
        pass

    def plot_fit(self, original_data, filtered_data, fitted_data, filename=None):
        """
        plots a fit and saves it in the output_dir or just shows it in the current pyplot environment
        """
        save_fig = plt.figure(figsize=(10, 5))
        save_ax = save_fig.add_subplot(111)
        save_ax.plot(original_data.freq,
                     fitted_data,
                     color='blue')
        save_ax.plot(original_data.freq,
                     original_data.amp,
                     color='orange',
                     alpha=0.5)
        save_ax.plot(filtered_data.freq,
                     filtered_data.amp,
                     color='red')
        self.save(save_fig, filename)
        plt.close(save_fig)

    def plot_fit_1arg(self, tuple_args):
        original_data, filtered_data, fitted_data, filename = tuple_args
        self.plot_fit(original_data, filtered_data, fitted_data, filename)

    @staticmethod
    def save(save_fig, filename):
        if filename:
            save_fig.savefig(os.path.join('{}.png'.format(filename)),
                             bbox_inches='tight')
        else:
            save_fig.show()

    def plot_param_vdc_function(self, param_array, vdc_array, filename=None):
        save_fig, save_ax = plt.subplots(1, 1)
        save_ax.plot(vdc_array, param_array)
        self.save(save_fig, filename)
        plt.close(save_fig)
