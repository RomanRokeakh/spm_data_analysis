import numpy as np


class Model:
    @staticmethod
    def amp_func(w, Amax, w_res, Q, A0):
        """
        static method
        model used for fitting
        takes as input physical values
        :return:
        amplitude at given frequency
        """
        return A0 + (((Amax - A0) * w_res ** 2 / Q) /
                     np.sqrt((w_res ** 2 - w ** 2) ** 2 + (w_res * w / Q) ** 2))

    @staticmethod
    def phase_func(w, w_res, Q):
        # return np.arctan(w * w_res / (Q * (np.square(w) - np.square(w_res))))
        return np.arctan(
            np.divide(
                (w * w_res),
                (Q * (np.square(w) - np.square(w_res))),
                out=np.zeros_like(w),
                where=w != w_res
            )
        )  # не понимаю, почему, но эта функция дает правильную форму

    @staticmethod
    def fix_A0(A0):
        return lambda w, Amax, w_res, Q: Model.amp_func(w=w, Amax=Amax, w_res=w_res, Q=Q, A0=A0)

    @staticmethod
    def fix_Amax(Amax):
        return lambda w, w_res, Q, A0: Model.amp_func(w=w, Amax=Amax, w_res=w_res, Q=Q, A0=A0)


class ModelParams:
    def __init__(self, w=None, Amax=None, w_res=None, Q=None, A0=None, phase=None):
        self.w = w
        self.Amax = Amax
        self.w_res = w_res
        self.Q = Q
        self.A0 = A0
        self.phase = phase

    def as_list(self):
        return self.Amax, self.w_res, self.Q, self.A0, self.phase

    @staticmethod
    def params_list():
        return ['Amax', 'w_res', 'Q', 'A0', 'phase']

    def __repr__(self):
        params = ',\n'.join([
            'Amax={}'.format(self.Amax),
            'w_res={}'.format(self.w_res),
            'Q={}'.format(self.Q),
            'A0={}'.format(self.A0),
            'phase={}'.format(self.phase)
        ])
        return 'ModelParams({})'.format(params)
