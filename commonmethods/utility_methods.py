import os
import glob
import re
import numpy as np


def read_data(filename):
    numbers = np.fromfile(filename, np.dtype('<f'))
    array = np.reshape(numbers, (3, len(numbers)//3))
    final_arr = np.zeros(len(array[0]), dtype=[('freq', '<f'), ('amp', '<f'), ('phase', '<f')])
    final_arr['freq'] = array[0]
    final_arr['amp'] = array[1]
    final_arr['phase'] = array[2]
    return final_arr


def read_floats(filename):
    with open(filename) as f:
        return list(map(float, f.read().split()))


def save_matrices_in_ascii(matrices, directory):
    for key in matrices:
        with open(os.path.join(directory, '{}.txt'.format(key)), 'w') as file:
            file.write('\n'.join(['\t'.join(map(str, line)) for line in matrices[key]]))


def save_params_as_matrices(params_dict, matrix_shape, output_path):
    num_of_elements = matrix_shape[0] * matrix_shape[1]
    matrices = {param: np.reshape(params_dict[param][:num_of_elements], matrix_shape)
                for param in params_dict}
    save_matrices_in_ascii(matrices, output_path)



def make_sure_dir_exists(dir_name):
    os.makedirs(dir_name, exist_ok=True)


def get_bin_files_in_dir(dir_name):
    files = glob.glob(os.path.join(dir_name, '*.bin'))
    return sorted(files, key=lambda path: os.path.getmtime(path))


def save_fit(filename, fit):
    with open(filename, 'w') as f:
        f.write(str(fit))


def save_fit_1arg(tuple_args):
    filename, fit = tuple_args
    save_fit(filename, fit)


def atoi(text):
    return int(text) if text.isdigit() else text


def natural_keys(text):
    return [atoi(c) for c in re.split('(\d+)', text)]


def list_dirs(dir_name):
    dirs = [d for d in (os.path.join(dir_name, d1) for d1 in os.listdir(dir_name)) if os.path.isdir(d)]
    dirs.sort(key=natural_keys)
    return dirs
