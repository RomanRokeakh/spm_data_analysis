import os
from commonmethods.utility_methods import list_dirs, natural_keys, save_params_as_matrices, read_floats
from common_classes import VdcProcessor, ModelParams, EFPFMSignal, OutputOptions
import numpy as np


class EFPFMProcessor:
    def __init__(self, input_dirs, output_dir, vdc, filtration_start,
                 output_options,
                 matrix_shape=None):
        self.vdc_processor = VdcProcessor(input_dirs, output_dir, vdc, filtration_start, output_options)
        if isinstance(input_dirs, str):
            self.dirs_to_process = list_dirs(input_dirs)
        else:
            self.dirs_to_process = input_dirs

        self.output_dirs = self.vdc_processor.output_dirs
        self.matrix_shape = matrix_shape
        if self.matrix_shape is not None:
            self.output_dirs['steps_matrices'] = os.path.join(self.output_dirs['steps_output'], 'matrices')
            self.output_dirs['zeros_matrices'] = os.path.join(self.output_dirs['zeros_output'], 'matrices')
        self.output_dirs['steps_mins'] = os.path.join(self.output_dirs['steps_output'], 'mins')
        self.output_dirs['zeros_mins'] = os.path.join(self.output_dirs['zeros_output'], 'mins')
        for key in self.output_dirs:
            os.makedirs(self.output_dirs[key], exist_ok=True)

    def process_dirs(self):
        self.vdc_processor.process(average_cycles=True)

    def process_mins(self):
        param_names = ModelParams.params_list()
        d = {'Amax': [], 'volts': []}
        volts = self.vdc_processor.vdc.voltage

        for dir_mod in ['steps', 'zeros']:
            amax_dir = self.output_dirs['{}_vals_{}'.format(dir_mod, 'Amax')]
            min_index = []
            for file in sorted(os.listdir(amax_dir), key=natural_keys):
                file_path = os.path.join(amax_dir, file)
                if self.vdc_processor.vdc.number_of_cycles > 1:
                    file_path = os.path.join(file_path, 'cycle 0.txt')
                array = read_floats(os.path.join(amax_dir, file, file_path))
                min_amp = np.argmin(array)
                min_index.append(min_amp)
                d['Amax'].append(array[min_amp])
                d['volts'].append(volts[min_amp])
            for param in param_names[1:]:
                d[param] = []
                param_dir = self.output_dirs['{}_vals_{}'.format(dir_mod, param)]
                files = sorted(os.listdir(param_dir), key=natural_keys)
                for i, file in enumerate(files):
                    file_path = os.path.join(param_dir, file)
                    if self.vdc_processor.vdc.number_of_cycles > 1:
                        file_path = os.path.join(file_path, 'cycle 0.txt')
                    array = read_floats(file_path)
                    d[param].append(array[min_index[i]])
            output_mins_dir = os.path.join(self.output_dirs['{}_mins'.format(dir_mod)])
            for param in param_names + ['volts']:
                with open(os.path.join(output_mins_dir, '{}.txt'.format(param)), 'w') as f:
                    f.write(' '.join(map(str, d[param])))
            if self.matrix_shape is not None:
                save_params_as_matrices({'Amax': d['Amax'], 'volts': d['volts']}, self.matrix_shape, self.output_dirs['{}_matrices'.format(dir_mod)])


if __name__ == '__main__':
    input_dirs = sorted(list_dirs(r'C:\Users\ru\Downloads\PPLN_with_graphen\PPLN_with_graphen'), key=natural_keys)
    #input_dirs = sorted(list_dirs(r'C:\Users\ru\Downloads\PPLN pure\Normal'), key=natural_keys)
    #output_dir = r'C:\Users\ru\Documents\git\spm_data_analysis\computation_results\ppln_pure_25062018'
    output_dir = r'C:\Users\ru\Documents\git\spm_data_analysis\computation_results\ppln_with_graphen_25062018'
    main_vdc = EFPFMSignal(steps_per_grows=20,
                           points_per_step=6,
                           points_per_zero=0,
                           number_of_cycles=2,
                           amp_start=0.5,
                           amp_end=0.5,
                           bipolar=1,
                           replace_zeros=True)
    filtration_start = 0
    processor = EFPFMProcessor(input_dirs=input_dirs,
                               output_dir=output_dir,
                               vdc=main_vdc,
                               filtration_start=filtration_start,
                               output_options=OutputOptions(
                                   Amax=True,
                                   w_res=True,
                                   phase=True,
                                   Q=True,
                                   A0=True,
                               ),
                               matrix_shape=(20, 20))
    #processor.process_dirs()
    processor.process_mins()
