import matplotlib.pyplot as plt
plt.style.use('ggplot')
from commonmethods.utility_methods import get_bin_files_in_dir
from common_classes import Filtrator, Parameters, Measurement
import os
import numpy as np


#input_dir = r'C:\Users\ru\Downloads\PPLN_with_graphen\PPLN_with_graphen\PPLN_with_graphen__10'
input_dir = r'C:\Users\ru\Downloads\PPLN pure\Normal\PPLN___250'
files_dir = os.path.join(input_dir, 'fourier_amp_ph')
files = get_bin_files_in_dir(
    files_dir
)
params_filename = os.path.join(input_dir, 'paramiters.txt')
parameters = Parameters.read_from_file(params_filename)
while True:
    file_number = np.random.randint(len(files))
    filename = files[file_number]
    data = Measurement.read_from_file(filename)
    filtrator = Filtrator(0, parameters.bins_number)

    h_plots = 3
    v_plots = int(np.ceil((parameters.bins_number+1) / h_plots))
    fig, ax = plt.subplots(v_plots, h_plots, figsize=(14,9))
    fig.suptitle(filename)

    for k in range(h_plots*v_plots):
        i = k % h_plots
        j = k // h_plots
        if k > parameters.bins_number:
            ax[j, i].axis('off')
            continue

        filt_start = k
        
        filtered = filtrator.filter_measurement(data, filt_start)
        title = str(filt_start)
        if np.max(data.amp) in filtered.amp:
            title += '*'
        ax[j,i].set_title(title)
        
        ax[j,i].plot(data.freq, data.amp, color='blue')
        ax[j,i].plot(filtered.freq, filtered.amp, color='orange')
        ax[j,i].xaxis.set_ticklabels([])
        ax[j,i].yaxis.set_ticklabels([])

    fig.subplots_adjust(left=0.1, bottom=0.1, hspace=0.4)

    plt.show(block=False)
    plt.waitforbuttonpress(timeout=120)
    a = input()
    plt.close()
    if a:
        print(a)
        break
