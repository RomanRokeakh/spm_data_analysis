from common_classes import VdcProcessor, OutputOptions, SSPFMSignal
from commonmethods.utility_methods import read_floats, save_params_as_matrices, list_dirs, natural_keys
from itertools import chain
import numpy as np
import os
from scipy.optimize import leastsq
from scipy.signal import medfilt


class HysteresisProcessor:
    def __init__(self, bias):
        self.bias = np.array(bias)
        self.r_plus_indices, self.r_minus_indices = self.get_branches_indices()  # CLOCKWISE
        #self.r_minus_indices, self.r_plus_indices = self.get_branches_indices()  # COUNTER-CLOCKWISE
        self.V = np.array(sorted(self.bias[self.r_plus_indices]))

    def get_branches_indices(self):
        r_s_plus = self.bias.argmax()
        r_s_minus = self.bias.argmin()
        r_plus_indices = list(range(r_s_plus, r_s_minus + 1))
        r_minus_indices = list(chain(range(1, r_s_plus + 1), range(r_s_minus, len(self.bias))))
        return r_plus_indices, r_minus_indices

    def get_loop_branches(self, response):
        arrays = []
        for indices in [self.r_plus_indices, self.r_minus_indices]:
            d = {self.bias[i]: response[i] for i in indices}
            array = np.array([val for key, val in sorted(d.items())])
            arrays.append(array)
        return arrays

    def process_loop(self, response, mode=None):
        parameters = {'R_init': response[0]}
        R_0_values = response[np.where(self.bias[1:] == 0)[0] + 1]
        parameters['R_0_plus'] = R_0_values[0]
        parameters['R_0_minus'] = R_0_values[1]
        R_plus, R_minus = self.get_loop_branches(response)

        close_to_zero_response = np.abs(response).argsort()
        left_side_zero_response = np.where(
            np.logical_and(
                self.bias in self.bias[close_to_zero_response],
                self.bias < 0))[0]
        right_side_zero_response = np.where(
            np.logical_and(
                self.bias in self.bias[close_to_zero_response],
                self.bias > 0))[0]
        parameters['V_minus'] = self.bias[left_side_zero_response].min() if \
            len(left_side_zero_response) > 0 else self.V[len(self.V) // 4]
        parameters['V_plus'] = self.bias[right_side_zero_response].max() if \
            len(right_side_zero_response) > 0 else self.V[-(len(self.V) // 4)]

        parameters.update(self.process_loop_statistically(R_plus, R_minus))
        est = []
        if mode == 'fit':
            est = self.process_loop_fitting(R_plus, R_minus, parameters)
            parameters.update(self.convert_fit_est_to_parameters(est))
        return parameters

    def process_loop_statistically(self, R_plus, R_minus, a=2):
        delta_minus = (R_plus - R_minus) / 2
        delta_plus = (R_plus + R_minus) / 2
        N = len(self.V)
        delta_minus_sum = delta_minus.sum()
        #A_s = ((self.V[-1] - self.V[0]) / N) * delta_minus_sum
        A_s = abs(((self.V[-1] - self.V[0]) / N) * delta_minus_sum)
        Im = (delta_minus * self.V).sum() / delta_minus_sum
        sigma = np.sqrt((delta_minus * np.square(self.V - Im)).sum() / delta_minus_sum)
        R_s_plus = np.nanmean(R_plus[np.where(self.V > Im + a * sigma)])
        R_s_minus = np.nanmean(R_minus[np.where(self.V < Im - a * sigma)])
        if np.isnan(R_s_minus):
            R_s_minus = R_plus[0]
        if np.isnan(R_s_plus):
            R_s_plus = R_plus[-1]
        return {
            'A_s': A_s,
            'Im': Im,
            'R_s_plus': R_s_plus,
            'R_s_minus': R_s_minus
        }

    def process_loop_fitting(self, R_plus, R_minus, parameters):
        def sum_of_squared_error(vector):
            a1, a2, a3, a4, a5, a6 = vector
            error1 = R_plus - self.r_plus(self.V, a1, a2, a3, a4, a5, a6)
            error2 = R_plus - self.r_plus(self.V, a1, a2, a3, a4, a5, a6)
            return np.power(error1, 2).sum() / np.power(error1, 2) + \
                   (np.power(error2, 2).sum() / np.power(error2, 2))
        est, _ = leastsq(func=sum_of_squared_error,
                         x0=np.array([
                             parameters['R_s_plus'],
                             parameters['R_s_minus'] - parameters['R_s_plus'],
                             parameters['V_minus'],
                             parameters['V_plus'],
                             parameters['V_minus'] - self.V[-1],
                             1
                      ]))
        return est

    def convert_fit_est_to_parameters(self, est):
        return {
            'R_s_plus': est[0],
            'R_s_minus': est[1] + est[0],
            'V_minus': est[2],
            'V_plus': est[3],
            'V_c_plus': est[2] - est[4],
            'V_c_minus': est[3] + est[4],
            'slope': est[5]
        }

    def r_plus(self, v, a1, a2, a3, a4, a5, a6):
        """
        :param v:
        :param a1: R_s_plus
        :param a2: R_s_minus - R_s_plus
        :param a3: V_minus
        :param a4: V_plus
        :param a5: V_minus - V_c_plus
        :param a6: V_c_minus - V_plus
        :return:
        """
        return a1 - a2 * (1 / (1 + np.power(np.e, (v - a3) / a5))) + v * a6

    def r_minus(self, v, a1, a2, a3, a4, a5, a6):
        return a1 - a2 * (1 / (1 + np.power(np.e, (v - a4) / a5))) + v * a6

    def response_from_amp_and_phase(self, amp, phase):
        return amp * np.sin(phase)


class SSPFMProcessor:
    def __init__(self,
                 input_dirs,
                 output_dir,
                 vdc,
                 filtration_start,
                 matrix_shape=None,
                 check_fits=False,
                 measurements_filt_window=None):
        output_options = OutputOptions(Amax=True, phase=True, fit_graphs=check_fits)
        self.vdc_processor = VdcProcessor(
            input_dirs,
            output_dir,
            vdc,
            filtration_start,
            output_options=output_options,
            filt_window=measurements_filt_window
        )
        self.hyst_p = HysteresisProcessor(np.array(vdc.step_values[vdc.get_cycle_step_indices(0)]))

        self.output_dirs = self.vdc_processor.output_dirs
        self.matrix_shape = matrix_shape
        if self.matrix_shape is not None:
            self.output_dirs['steps_matrices'] = os.path.join(self.output_dirs['steps_output'], 'matrices')
            self.output_dirs['zeros_matrices'] = os.path.join(self.output_dirs['zeros_output'], 'matrices')

        for dir_mod in ['steps', 'zeros']:
            self.output_dirs['{}_response_vals'.format(dir_mod)] = os.path.join(
                self.output_dirs['{}_vals'.format(dir_mod)],
                'response_sin'
            )
            self.output_dirs['{}_response_graphs'.format(dir_mod)] = os.path.join(
                self.output_dirs['{}_graphs'.format(dir_mod)],
                'response_sin'
            )
            self.output_dirs['{}_parameters'.format(dir_mod)] = os.path.join(
                self.output_dirs['{}_output'.format(dir_mod)],
                'parameters'
            )
        for key in self.output_dirs:
            os.makedirs(self.output_dirs[key], exist_ok=True)

    def process_dirs(self):
        self.vdc_processor.process(filt_window=1)

    def process_loops(self, medfilt_window=1, response_graphs=True, response_vals=True):
        for dir_mod in ['zeros', 'steps']:
            amp_dir = self.output_dirs['{}_vals_{}'.format(dir_mod, 'Amax')]
            phase_dir = self.output_dirs['{}_vals_{}'.format(dir_mod, 'phase')]
            mean_parameters = {'Amax': [], 'phase': []}
            for measurement_path in os.listdir(amp_dir):
                if self.vdc_processor.vdc.number_of_cycles == 1:
                    cycle_files = [measurement_path]
                else:
                    cycle_files = [os.path.join(measurement_path, file)
                                   for file
                                   in os.listdir(os.path.join(amp_dir, measurement_path))]
                response_cycles = []
                parameters_cycles = []
                needed_amp = []
                needed_phase = []
                for cycle_file in cycle_files:
                    amp = medfilt(read_floats(os.path.join(amp_dir, cycle_file)), medfilt_window)
                    phase = medfilt(read_floats(os.path.join(phase_dir, cycle_file)), medfilt_window)
                    needed_amp.append(amp[0])
                    needed_phase.append(phase[0])
                    response = self.hyst_p.response_from_amp_and_phase(amp, phase)
                    parameters = self.hyst_p.process_loop(response)
                    response_cycles.append(response)
                    parameters_cycles.append(parameters)
                for key in parameters_cycles[0]:
                    if key not in mean_parameters:
                        mean_parameters[key] = []
                    mean_parameters[key].append(
                        np.array([parameters[key] for parameters in parameters_cycles]).mean())
                mean_parameters['Amax'].append(np.array(needed_amp).mean())
                mean_parameters['phase'].append(np.array(needed_phase).mean())
                mean_response = np.array(response_cycles).mean(axis=0)
                if response_vals:
                    with open(
                            os.path.join(
                                self.output_dirs['{}_response_vals'.format(dir_mod)],
                                '{}.txt'.format(measurement_path)),
                            'w') as f:
                        f.write(' '.join(map(str, mean_response)))
                if response_graphs:
                    self.vdc_processor.dir_processor.plotter.plot_param_vdc_function(
                        mean_response,
                        self.hyst_p.bias,
                        os.path.join(
                            self.output_dirs['{}_response_graphs'.format(dir_mod)],
                            '{}.png'.format(measurement_path)
                        )
                    )
            for key, value in mean_parameters.items():
                with open(
                    os.path.join(
                        self.output_dirs['{}_parameters'.format(dir_mod)],
                        '{}.txt'.format(key)
                    ),
                    'w'
                ) as f:
                    f.write(' '.join(map(str, value)))
            if self.matrix_shape is not None:
                save_params_as_matrices(mean_parameters,
                                        self.matrix_shape,
                                        self.output_dirs['{}_matrices'.format(dir_mod)])


if __name__ == '__main__':
    input_dirs = list_dirs(r'C:\Users\ru\Downloads\good_2')
    output_dir = r'C:\Users\ru\Documents\git\spm_data_analysis\computation_results\good_2_27062018'
    main_vdc = SSPFMSignal(steps_per_grows=20,
                           points_per_step=4,
                           points_per_zero=4,
                           number_of_cycles=1,
                           amp_start=1,
                           amp_end=1,
                           bipolar=1,
                           replace_zeros=True)
    filtration_start = 8

    sspfm_p = SSPFMProcessor(input_dirs,
                             output_dir,
                             main_vdc,
                             filtration_start,
                             matrix_shape=(40, 40),
                             #measurements_filt_window={'amp': 1, 'phase': 3}
                             )

    #sspfm_p.process_dirs()
    sspfm_p.process_loops(3, response_graphs=True, response_vals=True)
