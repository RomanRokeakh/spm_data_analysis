import os
from commonmethods.utility_methods import save_matrices_in_ascii, get_bin_files_in_dir
from common_classes import Filtrator, Parameters, Fitter, Plotter, Measurement
import multiprocessing as mp
import numpy as np


class BEPFMProcessor:
    def __init__(self, input_dir, output_dir, filtration_start, save_graphs=None, matrix_shape=None):
        self.input_dir = input_dir
        self.output_dir = output_dir
        os.makedirs(self.output_dir, exist_ok=True)
        self.vals_dir = os.path.join(self.output_dir, 'fit_results')
        if matrix_shape:
            self.matrices_dir = os.path.join(self.output_dir, 'matrices')
            os.makedirs(self.matrices_dir, exist_ok=True)
            self.matrix_shape = matrix_shape
        else:
            self.matrices_dir = None
        if save_graphs:
            self.graphs_dir = os.path.join(self.output_dir, 'graphs')
            os.makedirs(self.graphs_dir, exist_ok=True)
        else:
            self.graphs_dir = None

        self.filtration_start = filtration_start

        self.parameters = Parameters.read_from_file(os.path.join(self.input_dir, 'paramiters.txt'))
        self.fitter = Fitter(self.parameters)
        self.filtrator = Filtrator(filtration_start, self.parameters.bins_number)
        self.plotter = Plotter()

    def process_dir(self, dir_name):
        filenames = get_bin_files_in_dir(os.path.join(dir_name, 'fourier_amp_ph'))
        with mp.Pool(mp.cpu_count()) as p:
            data_list = p.map(Measurement.read_from_file, filenames)
            filtered_data = p.map(self.filtrator.filter_measurement, data_list)
            fits = p.map(self.fitter.fit, filtered_data)

            if self.graphs_dir is not None:
                fitted_data = [self.fitter.eval_fitted_amp(data_list[i], fits[i]) for i in range(len(data_list))]
                tuples = [
                    (data_list[i],
                     filtered_data[i],
                     fitted_data[i],
                     os.path.join(self.graphs_dir, filenames[i]))
                    for i in range(len(fits))]
                p.map(self.plotter.plot_fit_1arg, tuples)

        a0 = [fit.A0 for fit in fits]
        amax = [fit.Amax for fit in fits]
        q = [fit.q for fit in fits]
        phase = [fit.phase for fit in fits]
        wres = [fit.w_res for fit in fits]
        params_dict = {'A0': a0, 'Amax': amax, 'Q': q, 'phase': phase, 'w_res': wres}

        self.save_vals(params_dict)

        if self.matrices_dir is not None:
            self.save_matrices()

    def save_matrices(self, params_dict):
        num_of_elements = self.matrix_shape[0] * self.matrix_shape[1]
        matrices = {param: np.reshape(params_dict[param][:num_of_elements], self.matrix_shape)
                    for param in params_dict}
        save_matrices_in_ascii(matrices, self.matrices_dir)

    def save_vals(self, params_dict):
        for key in range(params_dict):
            params_str = ' '.join([str(val) for val in params_dict[key]])
            param_filename = '{}.txt'.format(key)
            target = os.path.join(self.vals_dir, param_filename)
            with open(target, 'w') as f:
                f.write(params_str)

    def main(self):
        pass
